import React from 'react';
import { Image, Text, View, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';


const Upage = porps => {

    return (
     
        <View>
            <ImageBackground style={styles.back_img} source={require('../Pages/img/image5.png')}></ImageBackground>

            <View style={styles.up_button}>

                <TouchableOpacity>
                    <View style={styles.button_user}>
                        <Text>  پرداخت از طریق نام کاربری</Text>
                        <Image source={require('./img/image3.png')}></Image>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity>
                    <View style={styles.button_user}>
                        <Text>  پرداخت از طریق بارکد</Text>
                        <Image source={require('./img/image4.png')}></Image>
                    </View>
                </TouchableOpacity>

            </View>
            <View style={{ flexDirection: 'column', alignItems: 'center', opacity: 0.6, marginTop: 100 }}>
                <Text style={{ fontSize: 25 }} >سیستم موقعیت یاب شما غیرفعال است</Text>
                <Text style={{ fontSize: 15 }}>جهت مشاهده کسب وکار های اطراف  خود سیستم</Text>
                <Text style={{ fontSize: 15 }}>موقعیت یاب خود را فعال کنید</Text>
            </View>

            <TouchableOpacity>

                <View style={styles.ac_button}>
                    <Text style={{ textAlign: 'center', fontSize: 25 }}>فعال سازی</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.cart_button}>
                    <Image source={require('./img/image6.png')}></Image>
                    <Text>پرداخت</Text>
                </View>
            </TouchableOpacity>

            <View style={styles.up_button}>

            <TouchableOpacity onPress={() => porps.navigation.navigate('Pay')}>
                    <View style={styles.pay_button} >
                        <Image source={require('./img/image7.png')}></Image>
                        <Text>تراکنش</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => porps.navigation.navigate('Settings')}>
                    <View style={styles.set_button} >
                        <Image source={require('./img/image8.png')}></Image>
                        <Text>تنظیمات</Text>
                    </View>
                </TouchableOpacity>


            </View>

        </View>

    );
}
export default Upage;
const styles = StyleSheet.create({
    back_img: {
        alignSelf: 'center',
        position: 'absolute',
        height: 500,
        width: 500,
        margin: 200,
        opacity: 0.2,

    },
    up_button: {
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 80,
    },
    button_user: {

        flexDirection: 'row',
        backgroundColor: '#33d5bf',
        height: 50,
        width: 190,
        justifyContent: 'center',
        borderRadius: 50,
        padding: 10,
        margin: 5,


    },
    ac_button: {
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: '#33d5bf',
        height: 80,
        width: 250,
        borderRadius: 50,
        padding: 0,
        marginTop: 25,
    },
    cart_button: {
        flexDirection: 'column',
        alignSelf: 'center',
        alignItems: 'center',
        height: 150,
        width: 150,
        borderRadius: 100,
        marginTop: 40,
        padding: 25,
        borderColor: "#5dade2",
        borderWidth: 2,

    },
    pay_button: {
        alignItems: 'center',
        justifyContent: 'center',

        height: 90,
        width: 200,
        backgroundColor: '#5dade2',
        borderTopRightRadius: 0,

    },
    set_button: {

        alignItems: 'center',
        justifyContent: 'center',
        height: 90,
        width: 200,
        backgroundColor: '#5dade2',
        borderTopLeftRadius: 0,
    },
})