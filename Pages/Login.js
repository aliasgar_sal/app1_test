import React from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

const Login = porps => {
    return (
        <View style={styles.body}>
            <Text style={styles.texts}>ورودبه حساب کاربری</Text>

            <TouchableOpacity>
                <Image source={require('./img/image1.png')}></Image>

                <View style={styles.import_imageـbutton}>
                    <Text style={styles.import_imageـbutton_text}>  انتخاب عکس</Text>
                    <Image style={{ marginTop: 0 }} source={require('./img/image2.png')}></Image>
                </View>
            </TouchableOpacity>

            <View >
                <Text style={styles.userandpass}>نام کاربری</Text>
                <TextInput keyboardType={'default'} placeholder="UserName" autoCapitalize='none' style={styles.textinput_styleu}></TextInput>
                <Text style={styles.userandpass}>رمز عبور</Text>
                <TextInput placeholder="Password" secureTextEntry={true} autoCapitalize='none' style={styles.textinput_stylep}></TextInput>
            </View>

            <TouchableOpacity onPress={() => porps.navigation.navigate('Upage')}>
                <View style={styles.loginـbutton}>
                    <Text style={styles.import_imageـbutton_text}>ورود</Text>
                </View>
            </TouchableOpacity>


        </View>

    );
}
export default Login;
const styles = StyleSheet.create({
    body: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffff',
        alignItems: 'center',
        padding: 50,
    },
    texts: {
        height: 60,
        fontSize: 25,
        color: '#210B61',
    },
    import_imageـbutton: {
        flexDirection: 'row',
        backgroundColor: '#33d5bf',
        height: 40,
        width: 160,
        justifyContent: 'center',
        borderRadius: 50,
        padding: 5,
    },
    import_imageـbutton_text: {
        fontSize: 20,
    },
    loginـbutton: {
        flexDirection: 'row',
        backgroundColor: '#33d5bf',
        height: 60,
        width: 350,
        justifyContent: 'center',
        borderRadius: 50,
        padding: 15,
        marginTop: 250,

    },
    userandpass: {
        fontSize: 18,
        color: '#ccc',
        padding: 10,
        // alignItems: 'flex-end',
    },
    textinput_styleu: {
        height: 60,
        width: 350,
        borderColor: '#33d5bf',
        borderWidth: 2,
        fontSize: 30,
        borderRadius: 10,
        textAlign: 'center',

    },
    textinput_stylep: {
        height: 60,
        width: 350,
        borderColor: '#33d5bf',
        borderWidth: 2,
        fontSize: 30,
        borderRadius: 10,
        textAlign: 'center',


    },


})