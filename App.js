import React from 'react'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// import page

import Login from './Pages/Login'
import Upage from './Pages/Upage'
import Settings from './Pages/Settings'
import Pay from './Pages/Pay'


const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Upage" component={Upage} options={{ headerShown: false }} />
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="Pay" component={Pay} />


      </Stack.Navigator>
    </NavigationContainer>
  );
}